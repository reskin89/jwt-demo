//Imports and setup
const express = require('express')
const jwt = require('jsonwebtoken')
const dotenv = require("dotenv")
const bodyParser = require('body-parser')

const app = express()
const port = 3000

dotenv.config();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('json spaces', 2);

//Handler Functions
const generateAccessToken = (identifier) => {
  return jwt.sign(identifier, process.env.TOKEN_SECRET, { expiresIn: '300s'} )
}

const authenticateToken = (req,res,next) => {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]
  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    console.log(err)
    if (err) return res.sendStatus(403)
    req.user = user
    next()
  })
}

//Routes
app.post('/gentoken', (req,res) => {
  console.log(req.body)
  const token = generateAccessToken({ identifier: req.body.identifier })
  res.json(token)
})

app.get('/', authenticateToken, (req, res) => {
  res.json({
    message: "JWT Accepted"
  })
})

//Start Server
app.listen(port, () => {
  console.log(`Running application on localhost:${port}`)
})